// @ts-ignore
const STS = require('aws-sdk/clients/sts')
const sts = new STS()

module.exports = async (RoleArn, RoleSessionName) => {
  const assume = await sts.assumeRole({RoleArn, RoleSessionName}).promise()

  // Store credentials in an easy to use way
  return {
    accessKeyId: assume.Credentials.AccessKeyId,
    secretAccessKey: assume.Credentials.SecretAccessKey,
    sessionToken: assume.Credentials.SessionToken,
  }
}