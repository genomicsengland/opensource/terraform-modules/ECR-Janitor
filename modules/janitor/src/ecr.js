// @ts-ignore
const ECR = require('aws-sdk/clients/ecr')

const ecr = new ECR()

exports.getImages = async (repositoryNames) => {
  const ECRImages = []

  // Get a list of the Repositories in this account
  let repositories = []

  // If we dont already have a list of repositories, get a list of all of them
  console.log('Discovering Repositories...')
  const params = { repositoryNames }

  do {
    // query
    const repos = await ecr.describeRepositories(params).promise()

    // store our pagination token
    params.nextToken = repos.nextToken

    // merge in these repos
    repositories = repositories.concat(repos.repositories)
  } while(params.nextToken)

  // For each repository, get a list of all the images (tags)
  for(const repository of repositories) {
    console.log('Inspecting Repository: ', repository.repositoryName)

    const params = { repositoryName: repository.repositoryName}
    let imageIds = []
    do {
      // query
      try {
        const images = await ecr.listImages(params).promise()

        // Pagination Token
        params.nextToken = images.nextToken

        // merge
        imageIds = imageIds.concat(images.imageIds)
      } catch(err) {
        console.error(err)
        imageIds = []
      }
    } while(params.nextToken)

    // For each image, format our data a little for later in the process
    for(const image of imageIds) {

      const metadata = await ecr.describeImages({
        repositoryName: repository.repositoryName,
        imageIds: [image],
      }).promise()

      console.log(`ECR Image: ${repository.repositoryUri}:${image.imageTag} @${metadata.imageDetails[0].imagePushedAt}`)

      // Check for image tags with SHA
      const tagWithSha = image.imageTag ? `${image.imageTag}@${metadata.imageDetails[0].imageDigest}` : null;

      ECRImages.push(Object.assign(image, {
        repositoryName: repository.repositoryName,
        repositoryUri: repository.repositoryUri,
        imageTagUri: `${repository.repositoryUri}:${image.imageTag}`,
        imageDigestUri: `${repository.repositoryUri}:${image.imageDigest}`,
        imageTagWithShaUri: tagWithSha ? `${repository.repositoryUri}:${tagWithSha}` : null,
        imageDigest: image.imageDigest,
        imageTag: image.imageTag,
        pushDate: metadata.imageDetails[0].imagePushedAt.valueOf(),
        keep: false, // Assume we will delete unless we find a reason not to
      }));
    }
  }

  return ECRImages
}

exports.deleteImageBatch = async (repositoryName, imageIds) => {
  const params = {repositoryName, imageIds}

  console.log('Delete Params: ', params)

  return await ecr.batchDeleteImage(params).promise()
}