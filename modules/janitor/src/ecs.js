// @ts-ignore
const ECS = require('aws-sdk/clients/ecs')

exports.getClient = (credentials) => {
  const ecs = new ECS({credentials, maxRetries: 50})
  return ecs
}

exports.getTaskDefImages = async (ecs) => {
  const foundImages = []
  // Get a list of the TaskDefs in the account

  let taskDefArns = []
  const params = {}
  do {
    // query
    const taskDefs = await ecs.listTaskDefinitions(params).promise()

    // pagination
    params.nextToken = taskDefs.nextToken

    // merge
    taskDefArns = taskDefArns.concat(taskDefs.taskDefinitionArns)
    console.log('TaskDefs Found: ', taskDefArns.length)
  } while(params.nextToken != null)

  // Get the TaskDef details
  for(let taskDefArn of taskDefArns) {
    const taskDef = await ecs.describeTaskDefinition({
      taskDefinition: taskDefArn
    }).promise()

    // Add each image in the TaskDef in to our list
    for(let containerDef of taskDef.taskDefinition.containerDefinitions) {
      console.log(`ECS Image ${containerDef.image} found in ${taskDefArn}`)
      foundImages.push(containerDef.image)
    }
  }

  return foundImages
}