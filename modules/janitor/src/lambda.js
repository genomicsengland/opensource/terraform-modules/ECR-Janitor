// @ts-ignore
const Lambda = require('aws-sdk/clients/lambda')

exports.getClient = (credentials) => {
  return new Lambda({credentials})
}

exports.getFunctionImages = async (lambda) => {
  const foundImages = []

  // get a list of all the functions
  let functions = []
  const params = {}
  do {
    // Query
    const fns = await lambda.listFunctions(params).promise()

    // pagination
    params.Marker = fns.NextMarker

    // merge
    functions = functions.concat(fns.Functions)
  } while(params.Marker)

  // filter to only functions that have an image bundle
  functions = functions.filter(func => func.PackageType == 'Image')

  // Check every version of each function
  for(let func of functions) {
    // Get all versions of the function
    let versions = []
    const params = {}
    do {
      // Query
      const ver = await lambda.listVersionsByFunction({ FunctionName: func.FunctionName }).promise()

      // Paginate
      params.Marker = ver.NextMarker

      // merge
      versions = versions.concat(ver.Versions)
    } while(params.Marker)

    // filter to only versions containing an Image bundle
    const imageBundled = versions.filter(ver => ver.PackageType != 'Zip')

    // Get the details of this function version
    for(let version of imageBundled) {
      const config = await lambda.getFunction({
        FunctionName: version.FunctionName,
        Qualifier: version.Version
      }).promise()

      foundImages.push(config.Code.ImageUri)

      console.log(`Function Image ${config.Code.ImageUri} found in ${config.Configuration.FunctionName}:${config.Configuration.Version}`)
    }

  }

  return foundImages
}
