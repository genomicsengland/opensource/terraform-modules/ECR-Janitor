const assume = require('./assume')
const ecr = require('./ecr')
const ecs = require('./ecs')
const lambda = require('./lambda')

const findReferencedImage = async (accounts) => {
  let foundImages = []
  for(let accountId of accounts) {
    //assume role in each account
    console.log(`Interrogating account: ${accountId}...`)
    const credentials = await assume(`arn:aws:iam::${accountId}:role/${process.env.ROLE_NAME}`, 'ECR-Janitor')

    const ecsClient = ecs.getClient(credentials)
    foundImages = foundImages.concat(await ecs.getTaskDefImages(ecsClient))

    const lambdaClient = lambda.getClient(credentials)
    foundImages = foundImages.concat(await lambda.getFunctionImages(lambdaClient))
  }

  return foundImages
}

const removeImages = async (ecr, unreferencedImages) => {
  // create a mutable copy
  let images = unreferencedImages

  // Delete the images that are not being used
  while(images.length > 0) {
    // get a subset based upon the repository name of the first entry
    const subset = images.filter(image => image.repositoryName === images[0].repositoryName)

    // store the images in other repositories for later batch deletion
    images = images.filter(image => image.repositoryName !== images[0].repositoryName)

    // split to 100 items
    while (subset.length > 0) {
      var subsetslice
      subsetslice = subset.splice(0,99)

      // Create the BatchDelete params
      const result = await ecr.deleteImageBatch(
        subsetslice[0].repositoryName,
        subsetslice.map(image => ({
          imageTag: image.imageTag,
          imageDigest: image.imageDigest,
        })),
      )

      console.log('Delete result: ', result)
    }
  }
}

const keepRecentImages = (ECRImages, keepRecent) => {
  if(keepRecent <= 0) {
    // Exit early if there's nothing to remove
    return ECRImages
  }

  // Log what we're doing
  console.log(`Keeping the most recent ${keepRecent} images...`)

  // Sort images in to repository buckets
  const imagesByRepo = {}
  for(const image of ECRImages) {
    if(!imagesByRepo[image.repositoryName]) {
      imagesByRepo[image.repositoryName] = []
    }
    imagesByRepo[image.repositoryName].push(image)
  }

  // Sort each repository bucket by date
  for(const repo in imagesByRepo) {
    imagesByRepo[repo].sort((a, b) => b.pushDate - a.pushDate)
    // set the keep flag in the first "keepRecent" images (or as many as there are if there are less than "keepRecent")
    for(let i = 0; i < Math.min(keepRecent, imagesByRepo[repo].length); i++) {
      imagesByRepo[repo][i].keep = true
      console.log('Implicitly Keeping Recent Image: ', imagesByRepo[repo][i].repositoryUri, imagesByRepo[repo][i].imageTag, imagesByRepo[repo][i].imageDigest)
    }
  }

  // merge imageByRepo back into a single array
  const images = []
  for(const repo in imagesByRepo) {
    images.push(...imagesByRepo[repo])
  }

  return images
}

const keepLatest = (ECRImages) => {
  // Exit early if there's nothing to remove
  if(process.env.KEEP_LATEST !== 'true') {
    return ECRImages
  }

  // Log what we're doing
  console.log('Keeping images with tags containing "latest"...')

  // Set the keep flag on the latest image
  for(const image of ECRImages) {
    if(image.imageTag && image.imageTag.includes('latest')) {
      image.keep = true
      console.log('Implicitly Keeping "*latest*" Image: ', image.repositoryUri, image.imageTag, image.imageDigest)
    }
  }

  return ECRImages
}

const keepKeptDigests = (ECRImages) => {
  if(process.env.KEEP_DIGESTS !== 'true') {
    return ECRImages
  }

  // Log what we're doing
  console.log('Keeping images with the same digest as a kept image...')

  // Get a list of images that are being kept
  const keptImages = ECRImages.filter(image => image.keep)

  // Get a list of images that are not being kept
  let removableImages = ECRImages.filter(image => !image.keep)

  // for each of the keptImages
  for(const keptImage of keptImages) {
    // find the images that have the same digest
    const imagesWithSameDigest = removableImages.filter(image => image.imageDigest === keptImage.imageDigest)

    // mark all images with the same digest as kept
    for(const image of imagesWithSameDigest) {
      image.keep = true
      console.log('Implicitly Keeping Image with Previously Kept Digest: ', image.repositoryUri, image.imageTag, image.imageDigest)
    }

    // find images that dont have the same digest
    const imagesWithDifferentDigest = removableImages.filter(image => image.imageDigest !== keptImage.imageDigest)

    // merge the two lists back together
    removableImages = imagesWithDifferentDigest.concat(imagesWithSameDigest)
  }

  // return both kept and removable images
  return keptImages.concat(removableImages)
}

const keepFoundImages = (ECRImages, foundImages) => {
  if(foundImages.length === 0) {
    return ECRImages
  }

  console.log('Keeping images that are referenced in ECS and Lambda...')
  for(const image of ECRImages) {
    if(foundImages.includes(image.imageTagUri)) {
      image.keep = true
    }

    if(foundImages.includes(image.imageDigestUri)) {
      image.keep = true
    }

    if (foundImages.includes(image.imageTagWithShaUri)) {
      image.keep = true;
    }
    
    if(image.keep) {
      console.log('Explicitly Keeping Found Image: ', image.repositoryUri, image.imageTag, image.imageDigest)
    }
  }

  return ECRImages
}

exports.handler = async () => {
  let repositories = []

  if(process.env.REPOSITORIES && process.env.REPOSITORIES.length > 0) {
    repositories = process.env.REPOSITORIES.split(',')
  }

  // Get a list of all the images we own
  let ECRImages = await ecr.getImages(repositories)

  // Get the accounts we need to check in
  // @ts-ignore
  const accounts = process.env.ACCOUNTS.split(',')

  // Find all the references to images in ECS and Lambda
  const foundImages = await findReferencedImage(accounts)

  // Keep recent N images
  ECRImages = keepRecentImages(ECRImages, process.env.KEEP_RECENT)

  // Keep images where the tag contains 'latest'
  ECRImages = keepLatest(ECRImages)

  // Keep found images
  ECRImages = keepFoundImages(ECRImages, foundImages)

  // Keep images where another image with the same digest is being kept
  ECRImages = keepKeptDigests(ECRImages)

  // Get a list of images that are not being kept
  const removableImages = ECRImages.filter(image => !image.keep)

  // Get a list of images that are being kept
  const keptImages = ECRImages.filter(image => image.keep)

  console.log('Kept Images: ', keptImages)

  // Clean up the unreferenced images
  if(process.env.DRYRUN == 'false') {
    await removeImages(ecr, removableImages)
  } else {
    console.log('Dry Run:')

    // List each image that would be deleted
    for(let img of removableImages) {
      console.log('Would have removed: ', img.repositoryUri, img.imageTag, img.imageDigest)
    }
  }

  return true // Success
}
