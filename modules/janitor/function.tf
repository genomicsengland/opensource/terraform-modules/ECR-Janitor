data "archive_file" "janitor" {
  type        = "zip"
  source_dir  = "${path.module}/src"
  output_path = "janitor-bundle.zip"
}

resource "aws_lambda_function" "janitor" {
  function_name    = var.name
  description      = "Janitor function to clean up ECR images that are no longer required"
  filename         = data.archive_file.janitor.output_path
  source_code_hash = data.archive_file.janitor.output_base64sha256
  role             = aws_iam_role.janitor.arn
  handler          = "janitor.handler"
  runtime          = var.runtime
  architectures    = ["arm64"]
  timeout          = 300
  environment {
    variables = {
      ROLE_NAME    = var.read_only_role_name
      ACCOUNTS     = join(",", var.external_accounts)
      REPOSITORIES = join(",", var.repositories)
      DRYRUN       = var.dry_run
      KEEP_LATEST  = var.keep_latest
      KEEP_RECENT  = var.keep_recent
      KEEP_DIGESTS = var.keep_kept_digests
    }
  }
  tracing_config {
    mode = "Active"
  }
}

#tfsec:ignore:aws-cloudwatch-log-group-customer-key
resource "aws_cloudwatch_log_group" "logs" {
  name              = "/aws/lambda/${aws_lambda_function.janitor.function_name}"
  retention_in_days = 7
}
