resource "aws_cloudwatch_event_rule" "janitor" {
  name                = "ecr-janitor"
  description         = "Run ECR Janitor as per the schedule"
  schedule_expression = var.schedule
  is_enabled          = true
}

resource "aws_cloudwatch_event_target" "janitor" {
  rule      = aws_cloudwatch_event_rule.janitor.name
  target_id = "Trigger-ECR-Janitor"
  arn       = aws_lambda_function.janitor.arn
}

resource "aws_lambda_permission" "janitor" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.janitor.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.janitor.arn
}