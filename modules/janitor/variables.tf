variable "name" {
  type        = string
  description = "Name for the ECR Janitor function"
  default     = "ECR-Janitor"
}

variable "read_only_role_name" {
  type        = string
  description = "Name of the role to assume in accounts to be searched"
  default     = "ecr-janitor-read-only"
}

variable "repositories" {
  type        = list(string)
  description = "List of repositories to check (or blank for all repositories in this account)"
  default     = []
}
variable "external_accounts" {
  type        = list(string)
  description = "List of accounts to search (_this_ account will always be searched)"
  default     = []
}

variable "dry_run" {
  type        = bool
  description = "Should the function physically delete unreferrenced ECR Images?"
  default     = false
}

variable "schedule" {
  type        = string
  description = "AWS Cron Schedule for running the function"
  default     = "cron(0 3 * * ? *)"
}

variable "boundary_policy_arn" {
  type        = string
  description = "ARN of the policy to use as a boundary for the IAM role"
  default     = null
}

variable "runtime" {
  type        = string
  description = "node runtime version"
  default     = "nodejs16.x"
}

variable "keep_latest" {
  type        = bool
  description = "Never remove images tagged 'latest'"
  default     = false
}

variable "keep_recent" {
  type        = number
  description = "Keep the last N images regardless of tag (0 = dont explicitly keep recent images)"
  default     = 0
}

variable "keep_kept_digests" {
  type        = bool
  description = "Keep images where the digest matches a digest marked to be kept"
  default     = false
}