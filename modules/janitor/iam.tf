resource "aws_iam_role" "janitor" {
  name               = var.name
  assume_role_policy = data.aws_iam_policy_document.janitor_assume.json
  inline_policy {
    name   = "janitor"
    policy = data.aws_iam_policy_document.janitor.json
  }
  permissions_boundary = var.boundary_policy_arn
}

data "aws_iam_policy_document" "janitor_assume" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}


data "aws_iam_policy_document" "janitor" {
  statement {
    sid = "logging"
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
    #tfsec:ignore:aws-iam-no-policy-wildcards
    resources = ["arn:aws:logs:*:*:*"]
  }

  statement {
    sid       = "assume"
    actions   = ["sts:AssumeRole"]
    resources = [for acc in var.external_accounts : "arn:aws:iam::${acc}:role/${var.read_only_role_name}"]
  }

  statement {
    actions = [
      "ecr:DescribeRepositories",
      "ecr:DescribeImages",
      "ecr:ListImages",
      "ecr:BatchDeleteImage",
    ]
    #tfsec:ignore:aws-iam-no-policy-wildcards
    resources = ["*"]
  }
}