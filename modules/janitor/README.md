# ECR Janitor

[Home](../../README.md) | [ECR Janitor Role](../janitor-role/README.md)

The ECR Janitor module creates the ECR Janitor Lambda function and associated CloudWatch schedule.

## Example Usage

```hcl
module "janitor" {
  source = "../../modules/janitor"

  name                = "ECR-Janitor"
  external_accounts   = concat([data.aws_caller_identity.current.account_id],var.other_accounts)
  repositories        = [var.ecr_repository_name]
  read_only_role_name = var.janitor_role_name
  schedule            = "cron(0 3 * * ? *)"
  dry_run             = true
}
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| dry\_run | Whether to run the janitor in dry-run mode. | `bool` | `false` | no |
| external\_accounts | List of external AWS account IDs to allow the janitor to access. | `list(string)` | n/a | yes |
| name | Name of the Lambda function. | `string` | n/a | yes |
| read\_only\_role\_name | Name of the read-only IAM role to use for the Lambda function. | `string` | n/a | yes |
| repositories | List of ECR repositories to clean up. | `list(string)` | n/a | yes |
| schedule | Schedule expression for the CloudWatch event. | `string` | n/a | yes |
| boundary\_policy\_arn | ARN of the IAM policy to use as a boundary for the Lambda function. | `string` | `""` | no |
| keep_latest | Never remove images tagged 'latest'. | `bool` | `false` | no |
| keep_recent | Keep the last N images regardless of tag (0 = dont explicitly keep recent images) | `number` | `0` | no |
| keep_kept_digests | Keep images where the digest matches a digest marked to be kept. | `bool` | `false` | no |

## Outputs

None.