

resource "aws_iam_role" "janitor" {
  name               = var.role_name
  assume_role_policy = data.aws_iam_policy_document.janitor-assume.json
  inline_policy {
    name   = "janitor"
    policy = data.aws_iam_policy_document.janitor.json
  }
  permissions_boundary = var.boundary_policy_arn
}

data "aws_iam_policy_document" "janitor-assume" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${var.janitor_account_id}:role/${var.assuming_role_name}"]
    }
  }
}

data "aws_iam_policy_document" "janitor" {
  statement {
    actions = [
      "ecs:ListTaskDefinitions",
      "ecs:DescribeTaskDefinition",
    ]
    #tfsec:ignore:aws-iam-no-policy-wildcards
    resources = ["*"]
  }

  statement {
    actions = [
      "lambda:GetFunction",
      "lambda:ListFunctions",
      "lambda:ListVersionsByFunction",
    ]
    #tfsec:ignore:aws-iam-no-policy-wildcards
    resources = ["*"]
  }
}