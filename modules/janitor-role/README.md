# ECR Janitor Role

[Home](../../README.md) | [ECR Janitor](../janitor/README.md)

The ECR Janitor Role module creates a role tht can be assumed byonly the ECR Janitor Lambda function.  This role allows the ECR Janitor to scan ECS Tasks and Lambda functions and delete images that are not in use.

## Example Usage
```hcl
module "janitor_role" {
  source = "../../modules/janitor-role"

  role_name          = data.terraform_remote_state.janitor.outputs.janitor_read_only_role
  assuming_role_name = data.terraform_remote_state.janitor.outputs.janitor_role
  janitor_account_id = data.terraform_remote_state.janitor.outputs.janitor_account
}
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| janitor\_account\_id | AWS account ID of the ECR Janitor. | `string` | n/a | yes |
| role\_name | Name of the IAM role to create. | `string` | n/a | yes |
| assuming\_role\_name | Name of the IAM role to assume. | `string` | n/a | yes |
| boundary\_policy\_arn | ARN of the IAM policy to use as a boundary for the role. | `string` | `""` | no |

## Outputs

None.
