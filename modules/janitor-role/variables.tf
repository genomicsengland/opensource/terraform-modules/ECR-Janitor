variable "janitor_account_id" {
  type        = string
  description = "Account ID where the ECR Janitor is running"
}

variable "assuming_role_name" {
  type        = string
  description = "Role name of the ECR Janitor Lambda function"
  default     = "ecr-janitor"
}

variable "role_name" {
  type        = string
  description = "Name of the role to be assumed by the Janitor to interrogate usage in an account"
  default     = "ecr-janitor-read-only"
}

variable "boundary_policy_arn" {
  type        = string
  description = "ARN of the policy to use as a boundary for the IAM role"
  default     = null
}