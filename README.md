# ECR Janitor

The ECR Janitor is composed from two separate modules, the first is the Janitor itself which scans ECR repositories for images, and then checks ECS and Lambda usage in all required accounts.

Images that are not in use are either automatically removed.  In dry-run mode portential deletions are reported in the logs, but not deleted from ECR.

The Janitor can either scan all ECR repositories in an account, or only specified ones.

The second module is used to create a role in target accounts.  This role is limited to only be assumable from the ECR-Janitor's own role and account to limit who can perform these operations

For further detail on each modules, see:

- [ECR Janitor](./modules/janitor/README.md)
- [ECR Janitor Role](./modules/janitor-role/README.md)

## Usage Options

### One ECR, Multiple Accounts

If you are using a single ECR repository, and want to scan all accounts, you need to deploy the `janitor-role` in all account.
You only need to deploy the `janitor` module once, and specify all accounts in the `external_accounts` variable.

### One ECR, One Account

If you use a single ECR repository, and want to scan only one account, you only need to deploy the `janitor` module, and do not use the `external_accounts` variable.
You will also need to deploy the `janitor-role` module.
