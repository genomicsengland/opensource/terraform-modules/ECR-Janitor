#/bin/env sh
# Simple wrapper script for Gitlab CI jobs

set -o pipefail

deploy_env=${deploy_env:=dev}
var_file=vars/${deploy_env}.tfvars
ch_dir=${ch_dir:=example}
scan_dir=${scan_dir}


function init() {
  # Allow the runner (and by extension Terraform) to clone private Gitlab projects
  git config --global credential.helper 'store --file ~/.git-credentials'
  echo -e "protocol=https\nhost=gitlab.com\nusername=gitlab-ci-token\npassword=${CI_JOB_TOKEN}" | git credential-store store
  terraform -chdir=${ch_dir} init -backend=false
}

function tfsec_scan() {
  tfsec --format default,junit --out tfsec.out --include-passed $scan_dir; exitcode=$? # Preserve exit code from tfsec scan for use after renaming report output
  mv tfsec.out.junit ${CI_JOB_ID}.tfsec.out.xml # Report must be .xml otherwise Gitlab returns 500 (https://docs.gitlab.com/ee/ci/unit_test_reports.html#how-to-set-it-up)

  exit $exitcode # Exit with the exitcode from tfsec scan
}

function fmt_check() {
  terraform fmt -check -recursive
}

function make_docs() {
  docker run --rm --volume "$(pwd):/terraform-docs" -u $(id -u) quay.io/terraform-docs/terraform-docs:0.16.0 markdown /terraform-docs > README.md
}

function validate() {
  init
  terraform -chdir=${ch_dir} validate
}

$1 ${@:2}