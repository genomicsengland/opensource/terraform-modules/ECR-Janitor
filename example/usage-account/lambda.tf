#tfsec:ignore:aws-lambda-enable-tracing
resource "aws_lambda_function" "lambda" {
  function_name = "ecr-janitor-lambda"
  description   = "ECR-Janitor testing lambda using an ECR image as its bundle"
  role          = aws_iam_role.lambda.arn
  package_type  = "Image"
  image_uri     = "${data.terraform_remote_state.janitor.outputs.ecr_url}:3.14"
}

resource "aws_iam_role" "lambda" {
  name               = "ecr-janitor-lambda"
  assume_role_policy = data.aws_iam_policy_document.lambda_assume.json
  inline_policy {
    name   = "ecr-janitor-lambda"
    policy = data.aws_iam_policy_document.lambda.json
  }
}

data "aws_iam_policy_document" "lambda_assume" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

# policy to allow the lambda to access the ECR repository
data "aws_iam_policy_document" "lambda" {
  statement {
    actions = [
      "ecr:GetDownloadUrlForLayer",
      "ecr:BatchGetImage",
      "ecr:BatchCheckLayerAvailability",
    ]
    #tfsec:ignore:aws-iam-no-policy-wildcards
    resources = ["*"]
  }

  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
    #tfsec:ignore:aws-iam-no-policy-wildcards
    resources = ["*"]
  }
}