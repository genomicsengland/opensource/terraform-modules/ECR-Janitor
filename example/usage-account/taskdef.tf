resource "aws_ecs_task_definition" "service" {
  family = "service"
  container_definitions = jsonencode(
    [
      {
        name      = "first"
        image     = "${data.terraform_remote_state.janitor.outputs.ecr_url}:3.16"
        cpu       = 10
        memory    = 512
        essential = true
      }
    ]
  )

}