data "terraform_remote_state" "janitor" {
  backend = "local"

  config = {
    path = "../ecr-account/terraform.tfstate"
  }
}