# The role assumed by the Janitor to discover image usage
module "janitor_role" {
  source = "../../modules/janitor-role"

  role_name          = data.terraform_remote_state.janitor.outputs.janitor_read_only_role
  assuming_role_name = data.terraform_remote_state.janitor.outputs.janitor_role
  janitor_account_id = data.terraform_remote_state.janitor.outputs.janitor_account
}