output "janitor_role" {
  value = module.janitor.role_name
}

output "janitor_read_only_role" {
  value = var.janitor_role_name
}

output "janitor_account" {
  value = data.aws_caller_identity.current.account_id
}

output "ecr_url" {
  value = aws_ecr_repository.ecr.repository_url
}