#tfsec:ignore:aws-ecr-enable-image-scans
#tfsec:ignore:aws-ecr-enforce-immutable-repository
#tfsec:ignore:aws-ecr-repository-customer-key
resource "aws_ecr_repository" "ecr" {
  name = var.ecr_repository_name
}

// Create permissions for the ECR repository to be accessed by var.other_accounts
resource "aws_ecr_repository_policy" "ecr" {
  repository = aws_ecr_repository.ecr.name
  policy     = data.aws_iam_policy_document.ecr.json
}

data "aws_iam_policy_document" "ecr" {
  statement {
    actions = [
      "ecr:GetDownloadUrlForLayer",
      "ecr:BatchGetImage",
      "ecr:BatchCheckLayerAvailability",
    ]
    principals {
      type        = "AWS"
      identifiers = [for acc in var.other_accounts : "arn:aws:iam::${acc}:root"]
    }
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}