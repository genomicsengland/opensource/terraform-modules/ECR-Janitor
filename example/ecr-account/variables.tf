variable "janitor_role_name" {
  default = "ecr-janitor-read-only"
}

variable "ecr_repository_name" {
  default = "janitor-ecr-test"
}

variable "other_accounts" {
  default = ["025403892336"]
}