# The role assumed by the Janitor to discover image usage
module "janitor_role" {
  source = "../../modules/janitor-role"

  /*
  role_name is the name of the role that the Janitor will assume.
  */
  role_name = var.janitor_role_name

  /*
  assuming_role_name is the name of the role that the Janitor will use to assume role_name.
  This limits who can make use of role_name to only the Janitor.
  */
  assuming_role_name = module.janitor.role_name

  /*
  janitor_account_id is the ID of the account that the Janitor is running in.
  */
  janitor_account_id = data.aws_caller_identity.current.account_id
}