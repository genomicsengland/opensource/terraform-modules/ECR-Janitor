# The Janitor module itself
module "janitor" {
  source = "../../modules/janitor"

  /*
  name is the resource name of the Janitor Lambda fuction.
  */
  name = "ECR-Janitor"

  /*
  external_accounts lists accounts that the Janitor should check for image usage.
  */
  external_accounts = concat([data.aws_caller_identity.current.account_id], var.other_accounts)

  /*
  repositories lists repositories that the Janitor should check for image usage.
  If empty, all ECR repositories in this account will be checked.
  */
  repositories = [var.ecr_repository_name]

  /*
  read_only_role_name is the name of the role that the Janitor will assume in target accounts.
  */
  read_only_role_name = var.janitor_role_name

  /*
  schedule is the AWS Cron Schedule for running the Janitor.
  Can be left black to use a default daily schedule
  */
  schedule = "cron(0 3 * * ? *)"

  /*
  dry_run determines whether the Janitor should actually delete images.
  this parameter defaults to false, and can be removed once you're confident
  that the Janitor is working as expected.
  */
  dry_run = true
}
