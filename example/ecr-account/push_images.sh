#!/bin/bash

SOURCE_IMAGE="ubuntu:bionic"

aws ecr get-login-password | docker login --username AWS --password-stdin 862987346819.dkr.ecr.eu-west-2.amazonaws.com/janitor-ecr-test

# Tag and push images
# bash for loop over range 1 to 20
for i in {1..20}
do
  # retag docker SOURCE_IMAGE to use ECR registry
  docker tag $SOURCE_IMAGE 862987346819.dkr.ecr.eu-west-2.amazonaws.com/janitor-ecr-test:3.$i
  # push image to ECR
  docker push 862987346819.dkr.ecr.eu-west-2.amazonaws.com/janitor-ecr-test:3.$i
done
