# App Stack

This is just to provide resources to test against.  Ideally we need more than one account, but for initial testing, this should be fine :)

Needs to contain:

- ECR Repo
- TaskDef
- Lambda Function using ECR Image as code bundle
- ECR-Janitor
- ECR-Janitor-Read-Role
